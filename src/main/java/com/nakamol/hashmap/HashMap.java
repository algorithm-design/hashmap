/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.nakamol.hashmap;

/**
 *
 * @author OS
 */
public class HashMap {

    private int key;
    private String value;
    private String[] FindLocate = new String[50];

    public HashMap(int key, String value) {
        this.key = key;
        this.value = value;
        FindLocate[hashmap(key)] = value;
    }

    private int hashmap(int key) {
        return key % FindLocate.length;
    }

    public void input(int key, String value) {
        FindLocate[hashmap(key)] = value;
    }

    public String get(int key) {
        return FindLocate[hashmap(key)];
    }

    public String remove(int index) {
        String hm = FindLocate[hashmap(index)];
        FindLocate[hashmap(index)] = null;
        return hm;
    }
}
