/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.hashmap;

/**
 *
 * @author OS
 */
public class TestHashMap {

    public static void main(String[] args) {

        HashMap hash = new HashMap(102, "Nakamol");
        System.out.println(hash.get(102));
        hash.input(127, "Buatib");
        System.out.println(hash.get(127));
        
        hash.remove(127);
        System.out.println(hash.get(127));
    }
}
